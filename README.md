ics-ans-eee-rootfs-mtca
=======================

Ansible playbook to install an EEE rootfs boot server for MicroTCA IOCs.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
