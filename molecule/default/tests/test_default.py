import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('eee_rootfs_mtca')


def test_etc_exports(host):
    exports = host.file('/etc/exports').content_string
    assert re.search(r'/export/nfsroots\s+\*\(async,ro,no_root_squash,insecure,crossmnt\)', exports) is not None
